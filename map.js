function map(elements, cb) {
  const modifiedArray = [];

  for (let index = 0; index < elements.length; index++) {
    let result = cb(elements[index], index, elements);
    modifiedArray.push(result);
  }

  return modifiedArray;
}

module.exports = map;
