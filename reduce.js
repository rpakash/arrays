function reduce(elements, cb, startingValue) {
  if (!Array.isArray(elements)) {
    return [];
  }
  let index = 0;
  if (startingValue === undefined) {
    startingValue = elements[0];
    index = 1;
  }

  for (index; index < elements.length; index++) {
    startingValue = cb(startingValue, elements[index], index, elements);
  }

  return startingValue;
}

module.exports = reduce;
