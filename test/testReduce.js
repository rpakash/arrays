const reduce = require("../reduce");

function sumNumbers(number1, number2) {
  return number1 + number2;
}

console.log(reduce([1, 2, 3, 4, 5, 5], sumNumbers));
