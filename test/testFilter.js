const filter = require("../filter");

function greaterThan2(element) {
  return element > 2;
}

console.log(filter([1, 2, 3, 4, 5, 5], greaterThan2));
