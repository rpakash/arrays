const each = require("../each");

function logElement(element, index) {
  console.log(index + ":" + element);
}

each([1, 2, 3, 4, 5, 5], logElement);
