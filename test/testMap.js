const map = require("../map");

function double(number) {
  return number * 2;
}

console.log(map([1, 2, 3, 4, 5, 5], double));
