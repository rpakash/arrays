const find = require("../find");

function greaterThan2(element) {
  if (element > 2) {
    return true;
  }

  return false;
}

console.log(find([1, 2, 3, 4, 5, 5], greaterThan2));
