function find(elements, cb) {
  for (let element of elements) {
    if (cb(element)) {
      return element;
    }
  }

  return undefined;
}

module.exports = find;
