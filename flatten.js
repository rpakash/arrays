function flatten(elements, depth) {
  if (!Array.isArray(elements)) {
    return [];
  }
  if (depth === undefined) {
    depth = 1;
  }

  if (depth <= 0) {
    return elements;
  }

  let finalArray = [];

  for (let element of elements) {
    if (Array.isArray(element)) {
      const reduceDepth = depth - 1;
      finalArray = finalArray.concat(flatten(element, reduceDepth));
    } else if (element !== undefined) {
      finalArray.push(element);
    }
  }

  return finalArray;
}

module.exports = flatten;
