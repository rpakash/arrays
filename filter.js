function filter(elements, cb) {
  let finalArray = [];

  if (!Array.isArray(elements)) {
    return finalArray;
  }

  for (let index = 0; index < elements.length; index++) {
    if (cb(elements[index], index, elements) === true) {
      finalArray.push(elements[index]);
    }
  }

  return finalArray;
}

module.exports = filter;
